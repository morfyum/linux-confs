#!/bin/bash
#	My Fedora Default Config
#####################################
# Exit when fails
set -o errexit
# Exit when use Undeclared variable
set -o nounset
# Set debuggin mod
set -x
#####################################
okOrNot='[OK]'

# =============== APPS ===============
echo "Installing recommended applications..."
	dnf install vim tilix
echo $okOrNot
echo "Installing small tools..."
	dnf install htop at
echo $okOrNot
echo "Installing filesystem tools..."
	dnf install exfat-utils fuse-exfat testdisk smartmontools
echo $okOrNot

echo "Adding RPMFusion repositories..."
# rmp -i add repos....
# ????
# Install Gstreamer codec packs for AAC
echo "[TODO]"


# clamav, (clamtk in rpmfusion-nonfree) 
: << 'clam-off'
echo "Installing security tools..."
echo "Install: clamav antivirus"
dnf install clamav
echo "[OK]"
echo "Install: clamav-update"
dnf install clamav clamav-update 
echo "Unset clamav-autoupdate service"
#https://askubuntu.com/questions/114000/how-to-update-clamav-definitions-database
# service clamav-freshclam stop
clam-off

#echo "Set Flathub repo to Gnome-Software" # OUTDATED?
# wget https://dl.flathub.org/repo/flathub.flatpakrepo


# =============== SYSTEM SETTINGS ===============
# /etc/sysctl.d/90-sysrq.conf
readonly sysctl='/etc/sysctl.conf'
echo "Set sysrq with sysctl"

echo "Write sysrq setting into /etc/sysctl.conf"
echo "Write swappiness = 7 into /etc/sysctl.conf"

echo "1" > /proc/sys/kernel/sysrq
sysctl -w kernel.sysrq=1

cat << EOM > /etc/sysctl.conf
kernel.sysrq = 1
vm.swappiness = 7
EOM
echo $okOrNot

# =============== GLOBAL ALIASES ===============
readonly globalrc='/etc/bashrc'
echo "Set $globalrc settings..."

cat << EOM > $globalrc
#All user prompt color
export PS1='\[\e[0;36m\][\u@\h \W]\$\[\e[0m\] '
#Aliases
alias vi='vim'
alias open='gedit
alias sudo='sudo '
alias fstype='lsblk -o NAME,SIZE,FSTYPE,MOUNTPOINT'
EOM

# =============== ROOT ALIASES ===============
readonly rootrc='/root/.bashrc'
echo "Set /root/.bashrc Aliases"

cat >> $rootrc <<EOL
#Root prompt color
export PS1='\[\e[1;31m\][\u@\h \W]\$\[\e[0m\] '
#Aliases
alias vi='vim'
alias fstype='lsblk -o NAME,SIZE,FSTYPE,MOUNTPOINT'
EOL
echo $okOrNot

# =============== USER ALIASES ===============
readonly userrc='~/.bashrc'
echo "Set $USER ( ~/.bashrc ) Aliases"

cat >> $userrc <<EOL
########## ##########
alias marsleep='echo "systemctl suspend" | sudo at'
EOL
echo $okOrNOt

# =============== APPLICATION CONFIGS ===============
echo "Set vim show numbers, and color: ron in /etc/vimrc"

cat >> /etc/vimrc <<EOL
set number
set tabstop=4
color ron
EOL
echo $okOrNot

echo "========== =========="
echo "WE ARE DONE... $okOrNot "
echo "========== =========="
