#!/bin/bash
# My Fedora Default Config scrip
#####################################
# Exit when fails
set -o errexit
# Exit when use Undeclared variable
set -o nounset
# Set debuggin mod
set -x
#####################################
BACKUPFOLDER='/opt/000-os-configs'
LONGTODAY=$(date +%Y-%m-%d-%Hh%Mm%Ss)
echo "Backup folder: $LONGTODAY"
mkdir -p $BACKUPFOLDER/$LONGTODAY/
echo -e "\n New backup in: " >> $BACKUPFOLDER/backup-log.log
echo "$BACKUPFOLDER/$LONGTODAY" >> $BACKUPFOLDER/backup-log.log

echo "========================"
ls -al $BACKUPFOLDER
echo "========================"

echo "==================== ===================="
echo "Welcome! Run this script as root, or use sudo command."
echo "Script interrupt is not recommended!"
echo "This script create backup fom modified config files"
echo "==================== ===================="

######################################
# =============== APPS ===============
# TODO: KODEKEK!
echo "Installing recommended applications..."
dnf install vim tilix

echo "Installing small, but useful tools..."
dnf install htop iftop at

echo "Installing filesystem tools..."
dnf install exfat-utils fuse-exfat testdisk smartmontools

###########################################
# =============== RPMFUSION ===============
echo "Do you want to enable rpmfusion-free repository? (Recommended)"
read -n1 -p " [Y/n] " rpmfusion_free
case $rpmfusion_free in  
	[yY]) 
		echo -e "\nInstall: rpmfusion-free\n"
		echo "TODO"
	;;  
	*)
		echo -e "\nSKIP: rpmfusion-free\n"
	;;  
esac

echo "Do you want to enable rpmfusion-nonfree repository? (Optional)"
read -n1 -p " [Y/n] " rpmfusion_nonfree
case $rpmfusion_nonfree in  
	[yY])
		echo -e "\nInstall: rpmfusion-nonfree\n"
		echo "TODO"
	;;  
	*)
		echo -e "\nSKIP: rpmfusion-nonfree\n"
	;;  
esac

# clamav, (clamtk in rpmfusion-nonfree) 
: << 'clam-off'
echo "Installing security tools..."
echo "Install: clamav antivirus"
dnf install clamav
echo "[OK]"
echo "Install: clamav-update"
dnf install clamav clamav-update 
echo "Unset clamav-autoupdate service"
#https://askubuntu.com/questions/114000/how-to-update-clamav-definitions-database
# service clamav-freshclam stop
clam-off

#echo "Set Flathub repo to Gnome-Software" # OUTDATED?
# wget https://dl.flathub.org/repo/flathub.flatpakrepo

#################################################
# =============== SYSTEM SETTINGS ===============
readonly sysctl='/etc/sysctl.conf'

echo "Creating backup..."
rsync -vaurR $sysctl $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."

# /etc/sysctl.d/90-sysrq.conf
echo "Set sysrq with sysctl"

echo "Write sysrq setting into /etc/sysctl.conf"
echo "Write swappiness = 7 into /etc/sysctl.conf"

echo "1" > /proc/sys/kernel/sysrq
sysctl -w kernel.sysrq=1

cat << EOM > /etc/sysctl.conf
kernel.sysrq = 1
vm.swappiness = 7
EOM
echo $okOrNot


#################################################
# =============== GLOBAL ALIASES ===============
# WRITE THIS FILE
readonly WRITE_001='/etc/bashrc'
# READ THIS FILE
readonly READ_001='../shared/001-etc-bashrch'
# IMPORTANT-LINES
readonly CONF_001='# CONFS-001-ETC\/BASHRC'
readonly GREP_001='# CONFS-001-ETC/BASHRC'
readonly ENDL_001='# CONFS-001-ETC\/BASHRC-END'

echo "Creating backup..."
rsync -vaurR $WRITE_001 $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."

#readonly globalrc='/etc/bashrc'
#echo "Set $globalrc settings..."
# IMPORTANT! Never modify or remove the stored strings $CONF_* and $ENDL_* from written files!
#

echo "Check script used before..."
if grep -Fxq "$GREP_001" $WRITE_001
then
    echo "Lines exist"
	echo "Cleaning rows..."
	# -i nélkül nem töröl, csak teszt outputot ad
	sed -i "/$CONF_001/,/$ENDL_001/d" $WRITE_001
else
    echo "Probably first time to use this script!"
	echo "Else, something is wrong, Check: $WRITE_001"
fi

echo -e "\nADD NEW LINES FROM: [ $READ_001 ] TO: [ $WRITE_001 ]\n"
cat $READ_001
cat $READ_001 >> $WRITE_001

<< --asd--
# A KÉT KIFEJEZÉS KÖZÖTTI TELJES SZAKASZ TÖRLI!!!
sed  "/# CONFS-001-etc\/bashrc/,/# END-CONFS-001-etc\/bashrc/d" ./values.txt
--asd--

#################################################
# =============== ROOT ALIASES ===============
# WRITE THIS FILE
readonly WRITE_002='/root/.bashrc'
# READ THIS FILE
readonly READ_002='../shared/002-root-bashrch'
# IMPORTANT-LINES
readonly CONF_002='# CONFS-002-ROOT-BASHRC'
readonly GREP_002='# CONFS-002-ROOT-BASHRC'
readonly ENDL_002='# CONFS-002-ROOT-BASHRC-END'

echo "Creating backup..."
rsync -vaurR $WRITE_002 $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."

# IMPORTANT! Never modify or remove the stored strings $CONF_* and $ENDL_* from written files!

echo "Check script used before..."
if grep -Fxq "$GREP_002" $WRITE_002
then
    echo "Lines exist"
	echo "Cleaning rows..."
	# -i nélkül nem töröl, csak teszt outputot ad
	sed -i "/$CONF_002/,/$ENDL_002/d" $WRITE_002
else
    echo "Probably first time to use this script!"
	echo "Else, something is wrong, Check: $WRITE_002"
fi

echo -e "\nADD NEW LINES FROM [ $READ_002 ] TO: [ $WRITE_002 ]\n"
cat $READ_002
cat $READ_002 >> $WRITE_002

##############################################
# =============== USER ALIASES ===============
# WRITE THIS FILE
readonly WRITE_003='/home/*/.bashrc'
# READ THIS FILE
readonly READ_003='../shared/003-users-bashrch'
# IMPORTANT-LINES
readonly CONF_003='# CONFS-003-USERS-BASHRC'
readonly GREP_003='# CONFS-003-USERS-BASHRC'
readonly ENDL_003='# CONFS-003-USERS-BASHRC-END'

echo "Creating backup..."
rsync -vaurR $WRITE_003 $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."

# IMPORTANT! Never modify or remove the stored strings $CONF_* and $ENDL_* from written files!

echo "Check script used before..."
if grep -Fxq "$GREP_003" $WRITE_003
then
    echo "Lines exist"
	echo "Cleaning rows..."
	# -i nélkül nem töröl, csak teszt outputot ad
	sed -i "/$CONF_003/,/$ENDL_003/d" $WRITE_003
else
    echo "Probably first time to use this script!"
	echo "Else, something is wrong, Check: $WRITE_003"
fi

echo -e "\nADD NEW LINES FROM: [ $READ_003 ] TO: [ $WRITE_003 ]\n"
cat $READ_003
cat $READ_003 >> $WRITE_003

#####################################################
# =============== APPLICATION CONFIGS ===============
# WRITE THIS FILE
readonly WRITE_004='/etc/vimrc'
# READ THIS FILE
readonly READ_004='../shared/004-etc-vimrc'
# IMPORTANT-LINES
readonly CONF_004='\" CONFS-004-ETC-VIMRC'
readonly GREP_004='" CONFS-004-ETC-VIMRC'
readonly ENDL_004='\" CONFS-004-ETC-VIMRC-END'

echo "Creating backup..."
rsync -vaurR $WRITE_004 $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."

# IMPORTANT! Never modify or remove the stored strings $CONF_* and $ENDL_* from written files!

echo "Check script used before..."
if grep -Fxq "$GREP_004" $WRITE_004
then
    echo "Lines exist"
	echo "Cleaning rows..."
	# -i nélkül nem töröl, csak teszt outputot ad
	sed -i "/$CONF_004/,/$ENDL_004/d" $WRITE_004
else
    echo "Probably first time to use this script!"
	echo "Else, something is wrong, Check: $WRITE_004"
fi

echo -e "\nADD NEW LINES FROM: [ $READ_004 ] TO: [ $WRITE_004 ]\n"
cat $READ_004
cat $READ_004 >> $WRITE_004

echo "========== =========="
echo "SCRIPT FINISHED! ;) "
echo "You found your backups in: $BACKUPFOLDER/$LONGTODAY"
echo "========== =========="
