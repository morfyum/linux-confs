#!/bin/bash
echo "TEST:"

BACKUPFOLDER='/opt/000-os-configs'
TODAY=$(date +%Y%m%d)
LONGTODAY=$(date +%Y-%m-%d-%Hh%Mm%Ss)
echo "MA: $TODAY"
echo "LONG: $LONGTODAY"
mkdir -p $BACKUPFOLDER/$LONGTODAY

echo -e "\n New BACKUP IN: " >> $BACKUPFOLDER/backup-log.log
echo "$BACKUPFOLDER/$LONGTODAY" >> $BACKUPFOLDER/backup-log.log

echo "========================"
ls -al $BACKUPFOLDER
echo "========================"


echo "Creating backup..."
rsync -vaurR /etc/bashrc $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."

echo "Creating backup..."
rsync -vaurR /root/.bashrc $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."
#rsync -vaurR /home/*/.bashrc $BACKUPFOLDER/$LONGTODAY

echo "Creating backup..."
rsync -vaurR /home/*/.bashrc $BACKUPFOLDER/$LONGTODAY/
echo "Modify configs..."

echo "You found your backups in: $BACKUPFOLDER/$LONGTODAY"

<< --yesOrNo--
#!/bin/bash
echo "Do you want to enable rpmfusion-free repository? (Recommended)"
read -n1 -p " [Y/n] " rpmfusion_free
case $rpmfusion_free in  
	[yY]) 
		echo -e "\nInstall: rpmfusion-free\n"
	;;  
	*)
		echo -e "\nSKIP: rpmfusion-free\n"
	;;  
esac

echo "Do you want to enable rpmfusion-nonfree repository? (Optional)"
read -n1 -p " [Y/n] " rpmfusion_nonfree
case $rpmfusion_nonfree in  
	[yY])
		echo -e "\nInstall: rpmfusion-nonfree\n"
	;;  
	*)
		echo -e "\nSKIP: rpmfusion-nonfree\n"
	;;  
esac
--yesOrNo--
