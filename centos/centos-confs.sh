#!/bin/bash

##################################################
echo "==============="
echo "ADD EXTRA REPOSITORIES..."
echo "==============="
echo "Install epel-relase repository"
yum install epel-release wget -y

##################################################
#SOURCE: https://rpmfusion.org/Configuration/
echo "Add rpmfusion-free repository"
wget https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm -P /tmp/
yum install /tmp/rpmfusion-free-release-7.noarch.rpm -y

echo "Do you want to add rpmfusion-nonfree to? y/N"; read t1
if [ "$t1" == "y" ];
then
	echo "Add rpmfusion-nonfree repository"
	wget https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm -P /tmp/
	yum install /tmp/rpmfusion-nonfree-release-7.noarch.rpm
else
        echo "SKIP"
fi
echo "[ OK ]"

##################################################
echo "==============="
echo "Addibg tilix repository"
echo "==============="
#SOURCE: https://www.tecmint.com/tilix-gtk3-tiling-terminal-emulator-for-linux/
touch /etc/yum.repos.d/tilix.repo

echo "[ivoarch-Tilix]" > /etc/yum.repos.d/tilix.repo
echo "name=Copr repo for Tilix owned by ivoarch" >> /etc/yum.repos.d/tilix.repo
echo "baseurl=https://copr-be.cloud.fedoraproject.org/results/ivoarch/Tilix/epel-7-\$basearch/" >> /etc/yum.repos.d/tilix.repo
echo "type=rpm-md" >> /etc/yum.repos.d/tilix.repo
echo "skip_if_unavailable=True" >> /etc/yum.repos.d/tilix.repo
echo "gpgcheck=1" >> /etc/yum.repos.d/tilix.repo
echo "gpgkey=https://copr-be.cloud.fedoraproject.org/results/ivoarch/Tilix/pubkey.gpg" >> /etc/yum.repos.d/tilix.repo
echo "repo_gpgcheck=0" >> /etc/yum.repos.d/tilix.repo
echo "enabled=1" >> /etc/yum.repos.d/tilix.repo
echo "enabled_metadata=1" >> /etc/yum.repos.d/tilix.repo

##################################################
echo "=============== ==============="
echo "INSTALL EXTRA PACKAGES..."
echo "=============== ==============="
echo "Install Numix themes"
yum install numix-gtk-theme.noarch numix-icon-theme -y
echo "[ OK ]"

echo "Install filesystem tools..."
yum install fuse-exfat.x86_64 exfat-utils.x86_64 ntfs-3g ntfsprogs -y
echo "[ OK ]"

echo "Install useful packages"
yum install htop ffmpeg tilix vim yumex -y
echo "[ OK ]"

##################################################
echo "==============="
echo "SET OS HOSTNAME..."
echo "==============="
echo "Type your os NEW NAME"
read centosname
hostnamectl set-hostname $centosname
echo "[OK] - You need restart bash! "

##################################################

<<MOVED-TO-MINIMAL

echo "==============="
echo "SET VI & VIM SETTINGS..."
echo "==============="

echo "set readable color to vim..."
echo "" >> /etc/vimrc
echo "\"============== centos-extras ===============" >> /etc/vimrc
echo "colorscheme ron" >> /etc/vimrc

echo "Add to /etc/bashrc alias vi to use vim"
echo "" >> /etc/bashrc
echo "#============== centos-extras ===============" >> /etc/bashrc
echo "alias vi='vim'" >> /etc/bashrc
echo "alias ll='ls -hal'" >> /etc/bashrc
echo "Add root color prompt"
echo "export PS1=\"\\e[0;31m[\\u@\\h \W]\\$ \\e[m\"" >> /etc/bashrc
echo "[ OK ]"

echo "#============== centos-extras ===============" >> /home/*/.bashrc
echo "Add user color prompt"
echo "export PS1=\"\\e[0;36m[\\u@\\h \W]\\$ \\e[m\"" >> /home/*/.bashrc
echo "[ OK ]"

MOVED-TO-MINIMAL

##################################################

echo "==============="
echo "NOT-SCRIPTED: Numix témát manuálisan kell beállítanod!"
echo "==============="
##################################################
yum autoremove -y 
