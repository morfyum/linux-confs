#!/bin/bash

echo "======================="
echo "REMOVE USELESS GNOME-APPS..."
echo "======================="
echo "WARNING: gnome-classic-session package is set to remove!"
yum remove gnome-software nautilus

echo "======================="
echo "STOP AND DISABLE NOT GUI-WANT SERVICES..."
echo "======================="
systemctl stop postfix.service
systemctl disable postfix.service

################################################
echo "If you disalbe abrtd.service your systemd dont send bug reports to developers"
echo "Do you want to disable Automatic Bug Report System? y/N"; read t1
if [ "$t1" == "y" ];
then
	echo "Remove ABR..."
	systemctl stop abrtd.service
	systemctl disable abrtd.service
else
        echo "Good Choice! ;) "
fi
echo "[ OK ]"

################################################
echo "If you disable kdump.service, your boot performance on hdd is improved with 1 minute."
echo "Do you want to disable kdump service? y/N"; read t2
if [ "$t2" == "y" ];
then
	echo "Disable kdump"
	systemctl stop kdump.service
	systemctl disable kdump.service
else
        echo "Good Chouice! ;)"
fi
echo "[ OK ]"
