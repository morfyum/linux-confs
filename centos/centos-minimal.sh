#!/bin/bash

echo "=============== ==============="
echo "INSTALL MINIMAL TOOLS FOR BE USEFUL"
echo "=============== ==============="

echo "INSTALL: RECOMMENDED"
yum install vim ifconfig bash-completion* ncdu -y 

echo "INSTALL: OPTIONAL"
#yum install git
#yum install whois
#yum install macchanger

echo "=============== ==============="
echo " FEATURES: BASH PROMPT COLOR, VIM SETTINGS, EXTRA ALIASES"
echo "=============== ==============="

##################################################
echo "==============="
echo "SET: READABLE COLORS TO VIM, ALIASIN VI TO VIM..."
echo "==============="

echo "" >> /etc/vimrc
cat >> /etc/vimrc <<VIM_SETTINGS
"========== centos-extras ==========
set number
set tabstop=4
color ron
VIM_SETTINGS

##################################################
echo "==============="
echo "ADD PROMPT COLORS..."
echo "==============="

echo "Add global prompt color"
echo "#============== centos-extras ===============" >> /etc/bashrc
echo "export PS1='\[\e[1;31m\][\u@\h \W]\$\[\e[0m\] " >> /etc/bashrc
echo "[ OK ]"

echo "Add root prompt color"
echo "#============== centos-extras ===============" >> /etc/bashrc
echo "export PS1='\[\e[1;31m\][\u@\h \W]\$\[\e[0m\] " >> /root/.bashrc
echo "[ OK ]"

echo "Add user prompt color"
echo "#============== centos-extras ===============" >> /home/*/.bashrc
echo "export PS1='\[\e[0;36m\][\u@\h \W]\$\[\e[0m\] " >> /home/*/.bashrc
echo "[ OK ]"

##################################################
echo "==============="
echo "ADD ALIASES TO /etc/bashrc ..."
echo "==============="

echo "" >> /etc/bashrc
echo "#============== centos-extras ===============" >> /etc/bashrc
echo "alias vi='vim'" >> /etc/bashrc
echo "alias vi='vim'" >> /home/*/.bashrc
echo "alias marssleep='echo \"systemctl suspend\" | sudo at'" >> /etc/bashrc

echo "alias ll='ls -hal'" >> /etc/bashrc

echo "alias marssleep='echo"systemctl suspend" sudo at '"
##################################################
yum autoremove -y 
