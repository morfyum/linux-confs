#!/bin/bash
echo "=============== ==============="
echo " CENTOS RECOMMENDED XFCE GUI \n INSTALLATION SCRIPT FOR OLD COMPUTERS"
echo "=============== ==============="

echo "==============="
echo "ADD: epel-relase repository"
echo "==============="
yum install epel-release wget -y

echo "==============="
echo "INSTALL: X11 and GNOME GDM with GNOME DESKTOP..."
echo "==============="
yum groupinstall "Server with GUI" -y

echo "==============="
echo "INSTALL: @xfce group..."
echo "==============="
yum install @xfce -y

echo "==============="
echo "INSTALL: xfce-gui gui-fix packages"
echo "==============="
yum install network-manager-applet NetworkManager.x86_64 xfce4-screenshooter -y

# wavelan for xfce panel plugin | netload for???
yum install xfce4-wavelan-plugin xfce4-netload-plugin -y 

echo "==============="
echo "ENABLE: graphical.target"
echo "==============="

systemctl set-default graphical.target

echo "START: Graphical.target..."
# systemctl isolate graphical.target


# START MENU WITH SEARCH BAR 
# yum install xfce4-whiskermenu-plugin.x86_64

# IF YOU NEED ADD FLATHUB REPO FOR FLATPAK
#flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

##################################################
yum autoremove -y 
